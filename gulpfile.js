var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    htmlmin = require('gulp-htmlmin'),
    sass = require('gulp-sass'),
    minify = require('gulp-minify')


gulp.task('sass', function () {
    return gulp.src("app/scss/*.scss")
        .pipe(sass({
            outputStyle: 'compact'
        }))
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream())
})

gulp.task('serve', gulp.series('sass', function () {
    browserSync.init({
        server: {
            baseDir: './app'
        }
    })

    gulp.watch("app/scss/*.scss", gulp.series('sass'))
    gulp.watch("app/*.html").on('change', browserSync.reload)
    gulp.watch("app/js/*.js").on('change', browserSync.reload)
}))

gulp.task('build', function () {
    gulp.src('app/*.html')
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest('dist'));

    gulp.src('app/scss/*scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(gulp.dest("dist/css"))

    gulp.src('app/js/*.js')
        .pipe(minify({
            ext: {
                min: [/^(.*)\.min.js$/, '$1.js']
            }
        }))
        .pipe(gulp.dest('dist/js'))

    gulp.src('app/glide/**/*')
        .pipe(gulp.dest('dist/glide'))
    gulp.src('app/img/*')
        .pipe(gulp.dest('dist/img'))
    return gulp.src('app/*.php')
        .pipe(gulp.dest("dist"))
})



gulp.task('default', gulp.series('serve'))