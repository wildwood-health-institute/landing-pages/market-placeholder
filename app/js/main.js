const glide = new Glide('.glide', {
    type: 'carousel',
    autoplay: 4000,
}).mount()

window.onresize = glide.refresh

document.querySelectorAll('a[href^="#"').forEach(a => {
    a.addEventListener('click', e => {
        e.preventDefault()

        const anchor = a.getAttribute('href')
        document.querySelector(anchor).scrollIntoView({
            behavior: 'smooth'
        })
    })
})

const overlay = document.getElementById("overlay")
const overlayDisplay = display => overlay.style.display = display ? "flex" : "none"
const clean = _ => {
    overlayDisplay(false)
    enableScroll()
}

document.querySelector('form.email').addEventListener('submit', e => {
    e.preventDefault()
    overlayDisplay(true)
    document.querySelector('textarea').focus()
    disableScroll()

    document.documentElement.addEventListener('keydown', e => {
        if ("Escape" === e.key && "flex" === overlay.style.display) {
            overlayDisplay(false)
            enableScroll()
        }
    })
})

document.querySelector('form.message').addEventListener('submit', e => {
    e.preventDefault()
    const form = e.target
    const email = document.querySelector('input[name="email"]')
    const message = document.querySelector('textarea[name="message"]')

    form.style.opacity = 0
    email.setAttribute('readonly', true)
    message.setAttribute('readonly', true)

    let data = {
        _target: "wildwoodhealth.org",
        form_id: "23",
        357: email.value,
        359: message.value,
    }

    fetch('https://api.wildwoodhealth.org/frm/entries', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    })
        .then(r => {
            clean()
            showMessage(200 == r.status)
        })
        .catch(r => {
            showMessage(false)
            console.error(r.error)
        })
})

document.querySelector('.close').addEventListener('click', e => {
    e.preventDefault()
    clean()
})

function showMessage(status) {
    const message = status ? "Thank you for your submission." : "Something went wrong please reload and try again."
    const p = document.createElement("p")
    p.append(document.createTextNode(message))
    p.classList.add(status ? "success" : "error")
    document.querySelector('form.email').replaceWith(p)

    setTimeout(() => {
        p.style.opacity = 1
    }, 100)
}


function preventDefault(e) {
    e = e || window.event
    if (e.preventDefault)
        e.preventDefault()
    e.returnValue = false
}

function preventDefaultForScrollKeys(e) {
    // left: 37, up: 38, right: 39, down: 40,
    // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
    const keys = {
        37: 1,
        38: 1,
        39: 1,
        40: 1
    }

    if (keys[e.keyCode] && "TEXTAREA" !== e.target.nodeName) {
        preventDefault(e)
        return false
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false)
    document.addEventListener('wheel', preventDefault, {
        passive: false
    }) // Disable scrolling in Chrome
    window.onwheel = preventDefault // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault // older browsers, IE
    window.ontouchmove = preventDefault // mobile
    document.onkeydown = preventDefaultForScrollKeys
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false)
    document.removeEventListener('wheel', preventDefault, {
        passive: false
    }) // Enable scrolling in Chrome
    window.onmousewheel = document.onmousewheel = null
    window.onwheel = null
    window.ontouchmove = null
    document.onkeydown = null
}